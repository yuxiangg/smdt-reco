# sMDT reconstruction and simulation codebase

This code is designed to reconstruct raw data files from the University of Michigan sMDT test chamber, as well as simulate the test chamber in Geant4.

It produces diagnostic plots of raw data, fits T0 from drift time spectra, auto-calibrates an RT function,
and produces a final resolution measurement of the chamber.

## Cloning this repository

Simply clone the repository with your preffered method (HTTPS, SSH, etc.).  i.e. run

```bash
git clone ssh://git@gitlab.cern.ch:7999/<YOUR CERN USERNAME>/smdt-reco.git
```

This package is standalone, so it does not run off of an AnalysisBase or Athena build.  The only dependencies are CMake, ROOT and Geant4, which are avaliable on cvmfs.

## Setting up and building the code

The code is compiled using cmake.  On a cvmfs mounted machine, simply source the setup script in the top level directory

```
cd smdt_reco
source setup.sh
```

This script will setup ROOT, CMake and Geant4, set environment variables, and create aliases to binaries that will be stored in your build area

Next, build the code

```
mkdir build
cd build
cmake ..
make
```

## Acquiring raw data

Raw data files are not tracked by git (for obvious reasons!).  They are hosted on /eos at /eos/user/k/kenelson/smdt-reco-files/raw.  They should be stored in a raw data file directory under the top level directory (on the same level as this README).  To download run

```
cd $SMDT_DIR
mkdir raw
cd raw
scp <YOUR_USERNAME>@lxplus.cern.ch:/eos/user/k/kenelson/smdt-reco-files/raw/<DESIRED_RAW_FILE> .
```

There are several raw data files on the order of a GB in this area.  At a minimum download the file Rt_BMG_6_1.dat, which is a txt file initialization of the r(t) function.

## Running the code and further documentation

A complete dOxygen generated documentation on running the code is avaliable at http://cern.ch/kenelson/documentation/smdt-reco

Summary of the results and methodology is avaliable on CDS in document ATL-COM-MUON-2020-045 and on the arxiv: https://arxiv.org/abs/2105.09263

### Step 1: Running the MC 

The MC estimation of multiple coulomb scattering is necessary to produce a resolution measurement.  Run this as:

```
cd smdt-reco
runBeam --conf conf/mc_XXX.conf
autoCalibration --conf conf/mc_XXX.conf
residuals --conf conf/mc_XXX.conf
residuals -h --conf conf/mc_XXX.conf
```

Replacing XXX with the run number.  For BIS2-6 chambers XXX = 010, 011, 012, 014, 015.

### Step 2: Copying Merging Raw Data Files

Run on the online monitor machine (change paths and filenames depending on if you are working on lxplus or umt3):

scp muonuser@192.168.2.10:/home/filar/mdtCsm/dat/filename.dat .
scp filename.dat username@lxplus.cern.ch:/path/to/raw/file/area

Now, back on lxplus or umt3:

/path/to/your/offline_merge/Increment run00xxxxxx_YYYYMMDD_1.dat
mv incremented.dat run00xxxxxx_YYYYMMDD_1_inc.dat
/path/to/your/offline_merge/Increment run00xxxxxx_YYYYMMDD_2.dat
mv incremented.dat run00xxxxxx_YYYYMMDD_2_inc.dat
/path/to/your/offline_merge/OfflineMerge run00xxxxxx_YYYYMMDD_1_inc.dat run00xxxxxx_YYYYMMDD_2_inc.dat
mv merged.dat run00xxxxxx_YYYYMMDD_merged.dat

### Step 3: Run the code

In this order, the following commands need to be run:

```
decodeRawData --conf conf/standard.conf --runN RUNNUMBER --inputFile FILENAME.root
doT0Fit --conf conf/standard.conf  --runN RUNNUMBER --inputFile FILENAME.root
autoCalibration --conf conf/standard.conf --runN RUNNUMBER
residuals --conf conf/standard.conf --runN RUNNUMBER
residuals -h  --conf conf/standard.conf --runN RUNNUMBER
resolution --conf conf/standard.conf --runN RUNNUMBER
efficiency --conf conf/standard.conf --runN RUNNUMBER
makeDBFile --conf conf/standard.conf --runN RUNNUMBER --name module<MODNUM>
```

The RUNNUMBER and FILENAME.root can be passed on the command line, to be used with the standard.conf file, or you can make a dedicated config file with these values hardcoded.  the name field is required for the makeDBFile command.