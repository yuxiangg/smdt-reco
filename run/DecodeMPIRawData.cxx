
// C++ includes
#include <stdio.h>
#include <vector>

// ROOT includes
#include "TString.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TH1.h"
#include "TString.h"

// MuonReco includes
#include "MuonReco/RecoUtility.h"
#include "MuonReco/Signal.h"
#include "MuonReco/EventID.h"
#include "MuonReco/Event.h"
#include "MuonReco/EventDisplay.h"
#include "MuonReco/Geometry.h"
#include "MuonReco/Hit.h"
#include "MuonReco/Cluster.h"
#include "MuonReco/TimeCorrection.h"
#include "MuonReco/ArgParser.h"
#include "MuonReco/ConfigParser.h"
#include "MuonReco/IOUtility.h"

#define SAVE_TRACKS_OUT_OF_ROOT
#define TOTAL_BIN_QUANTITY 1024

using namespace std;
using namespace MuonReco;

int main(int argc, char* argv[]) {
  
  ArgParser ap = ArgParser(argc, argv);


  if (ap.hasKey("--help")) {
    std::cout << "Usage: " << std::endl
	      << "decodeMPIRawData --conf CONFIG" << std::endl
	      << "                 [--runN RUNNUMBER]" << std::endl
	      << "                 [--f1   FILE1.root]" << std::endl
	      << "                 [--f2   FILE2.root]" << std::endl
              << "Note:" << std::endl
              << "RUNUMBER and FILENAME will override their definition" << std::endl
              << "in the config file.  " << std::endl
              << "FILE1.root and FILE2.root are the names of the files, not paths. " << std::endl
              << "The path is automatically resolved." << std::endl;
    return 1;
  }

  // set the variables

  ConfigParser cp = ConfigParser(ap.getTString("--conf"));

  int runN;
  if (ap.hasKey("--runN")) {
    runN = ap.getInt("--runN");
  }
  else {
    runN = cp.items("General").getInt("RunNumber");
  }

  TString f1name;
  if (ap.hasKey("--f1")) {
    f1name = ap.getTString("--f1");
  }
  else {
    f1name = cp.items("General").getStr("CSMFile1");
  }

  TString f2name;
  if (ap.hasKey("--f2")) {
    f2name = ap.getTString("--f2");
  }
  else {
    f2name = cp.items("General").getStr("CSMFile2");
  }

  // open input files
  TFile* f1 = TFile::Open(IOUtility::getRawInputFilePath(f1name).Data());
  TFile* f2 = TFile::Open(IOUtility::getRawInputFilePath(f2name).Data());

  // create output file  
  TFile *p_output_rootfile = new TFile(IOUtility::getDecodedOutputFilePath(runN), "RECREATE");

  // prepare file structure for event display
  TDirectory *event_track[2];
  char track_group_name[128];
  char output_filename[200];

  Geometry geo = Geometry();
  geo.Configure(cp.items("Geometry"));

  for (int iTDC = 0; iTDC < Geometry::MAX_TDC; iTDC++) {
    int hitL, hitC;
    geo.GetHitLayerColumn(iTDC, 0, &hitL, &hitC);
    std::cout << "TDC " << iTDC << " channel " << 0 << " mapped to " << " l=" << hitL << " c=" << hitC  << std::endl;
  }
  TimeCorrection tc = TimeCorrection(cp);
  tc.Read();
  static EventDisplay   ed = EventDisplay();
  RecoUtility ru = RecoUtility(cp.items("RecoUtility"));


  // initialize the histograms

  TH1F *p_hits_distribution[Geometry::MAX_TUBE_LAYER];
  char histogram_name[256];
  for (Int_t layer_id = 0; layer_id != Geometry::MAX_TUBE_LAYER; layer_id++) {
    sprintf(histogram_name, "layer_%d_hits_distribution", layer_id);
    p_hits_distribution[layer_id] = new TH1F(histogram_name, histogram_name, Geometry::MAX_TUBE_COLUMN,-0.5, Geometry::MAX_TUBE_COLUMN-0.5);
  }


  char directory_name[256];
  TDirectory *tdc_directory     [Geometry::MAX_TDC];

  TH1F *p_tdc_tdc_time_raw      [Geometry::MAX_TDC];
  TH1F *p_tdc_tdc_time_corrected[Geometry::MAX_TDC];
  TH1F *p_tdc_tdc_time_selected [Geometry::MAX_TDC];
  TH1F *p_tdc_adc_time          [Geometry::MAX_TDC];
  TH1F *p_tdc_adc_time_selected [Geometry::MAX_TDC];

  TH1F *p_tdc_time_raw          [Geometry::MAX_TDC][Geometry::MAX_TDC_CHANNEL];
  TH1F *p_tdc_time_corrected    [Geometry::MAX_TDC][Geometry::MAX_TDC_CHANNEL];
  TH1F *p_tdc_time_selected     [Geometry::MAX_TDC][Geometry::MAX_TDC_CHANNEL];
  TH1F *p_adc_time              [Geometry::MAX_TDC][Geometry::MAX_TDC_CHANNEL];
  TH1F *p_adc_time_selected     [Geometry::MAX_TDC][Geometry::MAX_TDC_CHANNEL];

  TString h_name;

  for (Int_t tdc_id = 0; tdc_id != Geometry::MAX_TDC; tdc_id++) {
    sprintf(directory_name, "TDC_%02d_of_%02d_Time_Spectrum", tdc_id,Geometry::MAX_TDC);
    tdc_directory[tdc_id] = p_output_rootfile->mkdir(directory_name);
    tdc_directory[tdc_id]->cd();

    h_name.Form("tdc_%d_tdc_time_spectrum_raw", tdc_id);
    p_tdc_tdc_time_raw[tdc_id] = new TH1F(h_name, h_name+";Time [ns];Entries/0.78125 ns",TOTAL_BIN_QUANTITY, -400, 400);

    h_name.Form("tdc_%d_tdc_time_spectrum_corrected", tdc_id);
    p_tdc_tdc_time_corrected[tdc_id] = new TH1F(h_name, h_name+";Time [ns];Entries/0.78125 ns",TOTAL_BIN_QUANTITY, -400, 400);

    h_name.Form("tdc_%d_tdc_time_spectrum_selected", tdc_id);
    p_tdc_tdc_time_selected[tdc_id] = new TH1F(h_name, h_name+";Time [ns];Entries/0.78125 ns",TOTAL_BIN_QUANTITY, -400, 400);

    h_name.Form("tdc_%d_adc_time_spectrum", tdc_id);
    p_tdc_adc_time[tdc_id] = new TH1F(h_name, h_name+";Wilkinson ADC [ns];Entries/0.78125 ns", TOTAL_BIN_QUANTITY / 2, 0, 400);

    h_name.Form("tdc_%d_adc_time_spectrum_selected", tdc_id);
    p_tdc_adc_time_selected[tdc_id] = new TH1F(h_name, h_name+";Wilkinson ADC [ns];Entries/1.5625 ns", TOTAL_BIN_QUANTITY / 2, 0, 400);

    for (Int_t channel_id = 0; channel_id != Geometry::MAX_TDC_CHANNEL; channel_id++) {
      h_name.Form("tdc_%d_channel_%d_tdc_time_spectrum_raw", tdc_id, channel_id);
      p_tdc_time_raw[tdc_id][channel_id] = new TH1F(h_name,h_name+";Time [ns];Entries/0.78125 ns", TOTAL_BIN_QUANTITY,-400, 400);

      h_name.Form("tdc_%d_channel_%d_tdc_time_spectrum_corrected", tdc_id, channel_id);
      p_tdc_time_corrected[tdc_id][channel_id] = new TH1F(h_name,h_name+";Time [ns];Entries/0.78125 ns", TOTAL_BIN_QUANTITY,-400, 400);

      h_name.Form("tdc_%d_channel_%d_tdc_time_spectrum_selected", tdc_id, channel_id);
      p_tdc_time_selected[tdc_id][channel_id] = new TH1F(h_name,h_name+";Time [ns];Entries/0.78125 ns",TOTAL_BIN_QUANTITY,-400, 400);

      h_name.Form("tdc_%d_channel_%d_adc_time_spectrum", tdc_id, channel_id);
      p_adc_time[tdc_id][channel_id] = new TH1F(h_name, h_name+";Wilkinson ADC [ns];Entries/0.78125 ns",TOTAL_BIN_QUANTITY / 2, 0, 400);

      h_name.Form("tdc_%d_channel_%d_adc_time_spectrum_selected", tdc_id, channel_id);
      p_adc_time_selected[tdc_id][channel_id] = new TH1F(h_name, h_name + ";Wilkinson ADC [ns];Entries/0.78125 ns",TOTAL_BIN_QUANTITY / 2, 0, 400);

    }
  } // end for: all TDC
  p_output_rootfile->cd();

  TH1I* recoStatus = new TH1I("recoStatus", "Status flag from reconstruction",
                              10,0,9);

  TH2D* hitByLC = new TH2D("hitByLC", "All hits on tubes",
                           Geometry::MAX_TUBE_COLUMN,-0.5,Geometry::MAX_TUBE_COLUMN-0.5,
                           Geometry::MAX_TUBE_LAYER,-0.5,Geometry::MAX_TUBE_LAYER-0.5);
  hitByLC->SetStats(0);
  TH2D* hitByLC_nonoise = new TH2D("hitByLC_nonoise", "All hits on tubes wth ADC > 105",
                                   Geometry::MAX_TUBE_COLUMN,-0.5,Geometry::MAX_TUBE_COLUMN-0.5,
                                   Geometry::MAX_TUBE_LAYER,-0.5,Geometry::MAX_TUBE_LAYER-0.5);
  hitByLC_nonoise->SetStats(0);

  TH2D* badHitByLC = new TH2D("badHitByLC", "Hits on tubes with ADC < 105",
                              Geometry::MAX_TUBE_COLUMN,-0.5,Geometry::MAX_TUBE_COLUMN-0.5,
                              Geometry::MAX_TUBE_LAYER,-0.5,Geometry::MAX_TUBE_LAYER-0.5);
  badHitByLC->SetStats(0);
  TH2D* goodHitByLC = new TH2D("goodHitByLC", "Hits on tubes that passed clustering",
                               Geometry::MAX_TUBE_COLUMN,-0.5,Geometry::MAX_TUBE_COLUMN-0.5,
                               Geometry::MAX_TUBE_LAYER,-0.5,Geometry::MAX_TUBE_LAYER-0.5);
  goodHitByLC->SetStats(0);

  int hitL, hitC;
  int status = 0;
  bool pass_event_check;
  unsigned int word = 0x00000000;
  unsigned int header_type;
  unsigned int bunch_id = 0;
  EventID currEventID;
  EventID prevEventID = EventID(0x00000000);
  vector<Signal> trigVec = vector<Signal>();
  vector<Signal>  sigVec = vector<Signal>();
  bitset<4> header;
  Signal sig;
  Event  event = Event();

  TTree* eTree = new TTree("eTree", "eTree");
  eTree->Branch("event", "Event", &event);
  cout << "Processing..." << endl;

  unsigned long total_triggers = 0;
  unsigned long total_events   = 0;
  unsigned long total_triggers_pass = 0;
  unsigned long total_events_pass = 0;
  unsigned long total_signals = 0;
  unsigned long total_signals_pass = 0;
  unsigned long total_events_fail  = 0;
  unsigned long event_print = 100;
  unsigned long plotted_pass = 0;
  unsigned long plotted_fail = 0;
  int nhits = 0;

  unsigned long maxEventCount = cp.items("General").getInt("MaxEventCount", 1e100, 0);

  int nloop = 0;

  bool didLoopInTree = 1;

  int LastUsedEntry[Geometry::MAX_TDC];

  for (int iTDC=0; iTDC < Geometry::MAX_TDC; ++iTDC) LastUsedEntry[iTDC] = 0;
  unsigned int _event_ID, _hit_channel, _hit_TDC_count, _hit_ADC_count;

  TTree* t_tdc = 0;

  // process the data
  while (nloop < maxEventCount && didLoopInTree) {
    ++nloop;
    word += 0x00000100;
    currEventID = EventID(word);

    didLoopInTree = 0;    

    // do some event building in sigvec and trigvec
    if (f1) {
      for (int iTDC=0; iTDC < Geometry::MAX_TDC; ++iTDC) {

	t_tdc = (TTree*)(f1->Get(TString::Format("card_tree_%d", iTDC)));

	if (t_tdc) {
	  // here we have one TDC to work with	  
	  std::vector<unsigned int> *hit_channel=0, *hit_TDC_count=0, *hit_ADC_count=0;
	  
	  t_tdc->SetBranchAddress("event_ID", &_event_ID);
	  t_tdc->SetBranchAddress("hit_channel", &hit_channel);
	  t_tdc->SetBranchAddress("hit_TDC_count", &hit_TDC_count);
	  t_tdc->SetBranchAddress("hit_ADC_count", &hit_ADC_count);
	  
	  for (int iEntry = LastUsedEntry[iTDC]; iEntry < t_tdc->GetEntries(); ++iEntry) {
	    t_tdc->GetEntry(iEntry);
	    didLoopInTree = 1;
	    
	    if (nloop == _event_ID) {
	      // here we have a hit that we want to use
	      LastUsedEntry[iTDC] = iEntry;
	      
	      for (size_t iHit = 0; iHit < hit_channel->size(); ++iHit) {
		_hit_channel = hit_channel->at(iHit);
		_hit_TDC_count = hit_TDC_count->at(iHit);
		_hit_ADC_count = hit_ADC_count->at(iHit);		
		// need to check for signals on inactive channels
		if (geo.IsActiveTDCChannel(iTDC, _hit_channel%24) && 
		    !(iTDC < 2 && _hit_channel%24<4)) {

		  //
		  // ^ temporary fix
		  //
		  sigVec.push_back(Signal(Signal::RISING, iTDC, _hit_channel%24, 0, nloop,
					  _hit_TDC_count*0.2, 1));
		  sigVec.push_back(Signal(Signal::FALLING, iTDC, _hit_channel%24, 0, nloop,
					  (_hit_TDC_count+_hit_ADC_count)*0.2, 0));
		}
	      }
	    }
	    else if (_event_ID > nloop) break; // event ID in tree is well-ordered
	    
	  } // loop over entries in the ttree	    
	  delete hit_channel;
	  delete hit_TDC_count;
	  delete hit_ADC_count;
	} //end if: found a TTree corresponding to a TDC card
      } // end for: loop over TDCs
    } // CSM 1
    if (f2) {
      TList* keylist = f2->GetListOfKeys();
      for (int iTDC=0; iTDC < Geometry::MAX_TDC; ++iTDC) {

	t_tdc = (TTree*)(f2->Get(TString::Format("card_tree_%d", iTDC)));

	if (t_tdc) {
	  // here we have one TDC to work with
	  std::vector<unsigned int> *hit_channel=0, *hit_TDC_count=0, *hit_ADC_count=0;
	  
	  t_tdc->SetBranchAddress("event_ID", &_event_ID);
	  t_tdc->SetBranchAddress("hit_channel", &hit_channel);
	  t_tdc->SetBranchAddress("hit_TDC_count", &hit_TDC_count);
	  t_tdc->SetBranchAddress("hit_ADC_count", &hit_ADC_count);
	  
	  for (int iEntry = LastUsedEntry[iTDC+Geometry::MAX_TDC/2]; iEntry < t_tdc->GetEntries(); ++iEntry) {
	    t_tdc->GetEntry(iEntry);
	    didLoopInTree = 1;
	    
	    if (nloop == _event_ID) {
	      // here we have a hit that we want to use
	      LastUsedEntry[iTDC+Geometry::MAX_TDC/2] = iEntry;
	      
	      for (size_t iHit = 0; iHit < hit_channel->size(); ++iHit) {
		_hit_channel = hit_channel->at(iHit);
		_hit_TDC_count = hit_TDC_count->at(iHit);
		_hit_ADC_count = hit_ADC_count->at(iHit);
		// need to check for signals on inactive channels

		if (geo.IsActiveTDCChannel(iTDC+Geometry::MAX_TDC/2, _hit_channel%24)) {

		  sigVec.push_back(Signal(Signal::RISING, iTDC, _hit_channel%24, 1, nloop,
					  _hit_TDC_count*0.2, 1));
		  sigVec.push_back(Signal(Signal::FALLING, iTDC, _hit_channel%24, 1, nloop,
					  (_hit_TDC_count+_hit_ADC_count)*0.2, 0));
		}
	      }
	    }
	    else if (_event_ID > nloop) break; // event ID in tree is well-ordered
	    
	  } // loop over entries in the ttree	    
	  delete hit_channel;
	  delete hit_TDC_count;
	  delete hit_ADC_count;
	} //end if: found a TTree corresponding to a TDC card
      } // end for: loop over TDCs
    }
    trigVec.push_back(Signal(Signal::RISING, geo.TRIGGER_MEZZ, geo.TRIGGER_CH,
			     0, nloop, 300, 1));
    trigVec.push_back(Signal(Signal::FALLING, geo.TRIGGER_MEZZ, geo.TRIGGER_CH,
			     0, nloop, 301, 0));

    if (!didLoopInTree) continue;

    // process the event

    if (total_events % event_print == 0) {
      std::cout << "Processing Event " << total_events << std::endl;
      if (TMath::Floor(TMath::Log10(total_events)) > TMath::Floor(TMath::Log10(event_print))) event_print*=10;
    }


    total_events++;
    event = Event(trigVec, sigVec, currEventID);
    nhits = ru.DoHitFinding(&event,   &tc, geo);
    ru.DoHitClustering(&event);
    pass_event_check = kTRUE;
    pass_event_check = ru.CheckEvent(event, &status);
    event.SetPassCheck(pass_event_check);
    event.CheckClusterTime();
    recoStatus->Fill(status);

    for (Hit h : event.WireHits()) {
      p_tdc_tdc_time_raw      [h.TDC()]->Fill(h.DriftTime());
      p_tdc_tdc_time_corrected[h.TDC()]->Fill(h.CorrTime());
      p_tdc_adc_time          [h.TDC()]->Fill(h.ADCTime());

      p_tdc_time_raw      [h.TDC()][h.Channel()]->Fill(h.DriftTime());
      p_tdc_time_corrected[h.TDC()][h.Channel()]->Fill(h.CorrTime());
      p_adc_time          [h.TDC()][h.Channel()]->Fill(h.ADCTime());

      geo.GetHitLayerColumn(h.TDC(), h.Channel(), &hitL, &hitC);

      // fill heatmap                                                                               
      hitByLC->Fill(hitC, hitL);
      if (h.ADCTime() < 105)
	badHitByLC->Fill(hitC, hitL);
      else
	hitByLC_nonoise->Fill(hitC, hitL);
    }

    if (pass_event_check) {
      eTree->Fill();

      for (Cluster c : event.Clusters()) {
	for (Hit h : c.Hits()) {
	  p_tdc_tdc_time_selected[h.TDC()]->Fill(h.CorrTime());
	  p_tdc_adc_time_selected[h.TDC()]->Fill(h.ADCTime());

	  p_tdc_time_selected[h.TDC()][h.Channel()]->Fill(h.CorrTime());
	  p_adc_time_selected[h.TDC()][h.Channel()]->Fill(h.ADCTime());

	  geo.GetHitLayerColumn(h.TDC(), h.Channel(), &hitL, &hitC);

	  goodHitByLC->Fill(hitC, hitL);
	  p_hits_distribution[hitL]->Fill(hitC);
	}
      }

      for (Hit h : event.TriggerHits()) {
	p_tdc_time_raw      [h.TDC()][h.Channel()]->Fill(h.DriftTime());
	p_adc_time          [h.TDC()][h.Channel()]->Fill(h.ADCTime());
      }
    }

    if (pass_event_check) {
      total_signals_pass += sigVec.size();
      total_events_pass++;
      total_triggers_pass++;
    }
    else {
      total_events_fail++;
    }
    // plot the event                                                                               
    // plot the first 100 events meeting and not meeting the pass event check criteria              
    if ((pass_event_check && plotted_pass < 100) || (!pass_event_check && plotted_fail < 100 && nhits > 0)) {

      if (pass_event_check) {
	sprintf(track_group_name, "events_passing");
	plotted_pass++;
      }
      else {
	sprintf(track_group_name, "events_failing");
	plotted_fail++;
      }

      if ((pass_event_check && plotted_pass == 1) || (!pass_event_check&& plotted_fail == 1) )
	event_track[(int)pass_event_check] = p_output_rootfile->mkdir(track_group_name);


      event_track[(int)pass_event_check]->cd();
      ed.SetOutputDir(IOUtility::join(IOUtility::getRunOutputDir(runN), track_group_name, kTRUE));
      ed.DrawEvent(event, geo, event_track[(int)pass_event_check]);
      ed.Clear();
    } // end if: pass event check for first 100 events                                              


    // clean cache                                                                                  
    sigVec.clear();
    trigVec.clear();

  }

  cout << "Decoding completed !" << endl;

  /* plot the time spectrum for leading and trailing edge */
  cout << "Making plots... " << endl;
  
  cout << endl << "Plotting hits distribution... " << endl;
  TCanvas *p_hits_together_canvas = new TCanvas("layer_distribution", "layer_distribution");
  char canvas_name[256];
  for (Int_t layer_id = Geometry::MAX_TUBE_LAYER - 1; layer_id != -1; layer_id--) {
    sprintf(canvas_name, "layer_%d_hits_distribution", layer_id);
    p_hits_together_canvas->cd();
    p_hits_distribution[layer_id]->SetLineColor(layer_id + 1);
    p_hits_distribution[layer_id]->Draw("same");
  }

  p_hits_together_canvas->BuildLegend(0.75, 0.75, 1, 1);
  p_hits_together_canvas->SaveAs(IOUtility::join(IOUtility::getRunOutputDir(runN), "layer_distributions.png"));



  TDirectory* metaPlots = p_output_rootfile->mkdir("composite");
  ed.SetOutputDir(IOUtility::getRunOutputDir(runN));
  ed.DrawTubeHist(geo, hitByLC,         metaPlots);
  ed.Clear();
  ed.DrawTubeHist(geo, hitByLC_nonoise, metaPlots);
  ed.Clear();
  ed.DrawTubeHist(geo, goodHitByLC,     metaPlots);
  ed.Clear();
  ed.DrawTubeHist(geo, badHitByLC,      metaPlots);
  ed.Clear();

  p_output_rootfile->cd();
  eTree->Write();
  hitByLC->Write();
  badHitByLC->Write();
  goodHitByLC->Write();
  recoStatus->Write();
  p_output_rootfile->Write();

  TH2D* hitByLCLog = (TH2D*)hitByLC->Clone("hitByLCLog");

  for (int bx = 1; bx <= hitByLCLog->GetNbinsX(); ++bx) {
    for (int by = 1; by <= hitByLCLog->GetNbinsY(); ++by) {
      if (hitByLCLog->GetBinContent(bx, by))
        hitByLCLog->SetBinContent(bx, by, TMath::Log(hitByLCLog->GetBinContent(bx, by)));
    }
  }
  ed.DrawTubeHist(geo, hitByLCLog, metaPlots);
  ed.Clear();

#ifdef SAVE_TRACKS_OUT_OF_ROOT
  TCanvas *p_output_canvas = new TCanvas("", "");
  TString dir_name;
  p_output_canvas->cd();
  for (Int_t tdc_id = 0; tdc_id != Geometry::MAX_TDC; tdc_id++) {
    if (geo.IsActiveTDC(tdc_id)) {
      tdc_directory[tdc_id]->cd();
      dir_name = TString::Format("TDC_%02d_of_%02d_Time_Spectrum", tdc_id, Geometry::MAX_TDC);

      p_tdc_tdc_time_raw[tdc_id]->Draw();
      p_output_canvas->SaveAs(IOUtility::join(IOUtility::join(IOUtility::getRunOutputDir(runN), dir_name, kTRUE), TString::Format("tdc_%d_tdc_time_spectrum_raw.png", tdc_id)));

      p_tdc_tdc_time_corrected[tdc_id]->Draw();
      p_output_canvas->SaveAs(IOUtility::join(IOUtility::join(IOUtility::getRunOutputDir(runN), dir_name, kTRUE), TString::Format("tdc_%d_tdc_time_spectrum_corrected.png", tdc_id)));

      p_tdc_tdc_time_selected[tdc_id]->Draw();
      p_output_canvas->SaveAs(IOUtility::join(IOUtility::join(IOUtility::getRunOutputDir(runN), dir_name, kTRUE), TString::Format("tdc_%d_tdc_time_spectrum_selected.png", tdc_id)));

      p_tdc_adc_time[tdc_id]->Draw();
      p_output_canvas->SaveAs(IOUtility::join(IOUtility::join(IOUtility::getRunOutputDir(runN), dir_name, kTRUE), TString::Format("tdc_%d__adc_time_spectrum.png", tdc_id)));

      p_tdc_adc_time_selected[tdc_id]->Draw();
      p_output_canvas->SaveAs(IOUtility::join(IOUtility::join(IOUtility::getRunOutputDir(runN), dir_name, kTRUE), TString::Format("tdc_%d__adc_time_spectrum_selected.png", tdc_id)));
      for (Int_t channel_id = 0; channel_id != Geometry::MAX_TDC_CHANNEL;channel_id++) {
        if (geo.IsActiveTDCChannel(tdc_id, channel_id)) {

          p_tdc_time_raw[tdc_id][channel_id]->Draw();
          p_output_canvas->SaveAs(IOUtility::join(IOUtility::join(IOUtility::getRunOutputDir(runN), dir_name, kTRUE), TString::Format("tdc_%d__channel_%d__tdc_time_spectrum_raw.png", tdc_id, channel_id)));

          p_tdc_time_corrected[tdc_id][channel_id]->Draw();
          p_output_canvas->SaveAs(IOUtility::join(IOUtility::join(IOUtility::getRunOutputDir(runN), dir_name, kTRUE), TString::Format("tdc_%d__channel_%d__tdc_time_spectrum_corrected.png", tdc_id, channel_id)));

          p_tdc_time_selected[tdc_id][channel_id]->Draw();
          p_output_canvas->SaveAs(IOUtility::join(IOUtility::join(IOUtility::getRunOutputDir(runN), dir_name, kTRUE), TString::Format("tdc_%d__channel_%d__tdc_time_spectrum_selected.png", tdc_id, channel_id)));

          p_adc_time[tdc_id][channel_id]->Draw();
          p_output_canvas->SaveAs(IOUtility::join(IOUtility::join(IOUtility::getRunOutputDir(runN), dir_name, kTRUE), TString::Format("tdc_%d__channel_%d__adc_time_spectrum.png", tdc_id, channel_id)));

          p_adc_time_selected[tdc_id][channel_id]->Draw();
          p_output_canvas->SaveAs(IOUtility::join(IOUtility::join(IOUtility::getRunOutputDir(runN), dir_name, kTRUE), TString::Format("tdc_%d__channel_%d__adc_time_spectrum_selected.png", tdc_id, channel_id)));
        }
      }
    } // end if: active TDC
  }
#endif

  int nEntries = eTree->GetEntries();

  delete p_output_rootfile;

  cout << endl;
  cout << "Total Triggers: " << total_triggers << endl;
  cout << "Pass  Triggers: " << total_triggers_pass << endl;
  cout << endl;
  cout << "Total Events:   " << total_events   << endl;
  cout << "Pass  Events:   " << total_events_pass << endl;
  cout << endl;
  cout << "Total Signals:  " << total_signals  << endl;
  cout << "Pass  Signals:  " << total_signals_pass << endl;
  cout << endl;
  cout << "N tree entries: " << nEntries << endl;

  gROOT->SetBatch(kFALSE);
  return 0;
}
